#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>
#include <fcntl.h>
#include <unistd.h>
#include <inttypes.h>
#include "millerRabin.h"

#define CYPHERTEXT "ctext.txt"
#define PLAINTEXT "ptext.txt"
#define OUTPUT "dtext.txt"

void keygen(uint64_t seed, char* argv[]){
    //./crypto publickey.text privatekey.txt seed -k
    //
    
    srand(seed);
    // choose 2 as the generator and pick a prime from that
    uint64_t g = 2;
    uint64_t p = 0;

    do {
        // range is to ensure p has a high bit of 1
        uint64_t q = randBetween(UINT64_MAX >> 2, UINT64_MAX >> 1);
        if (q % 12 != 5 || !millerRabin(q, 20)) {
            continue;
        }
        p = 2 * q + 1;
        // p must be prime and have a high bit of 1
    } while (p <= (UINT64_MAX >> 1) || !millerRabin(p,16));
    
    uint64_t d = randBetween(1, p - 1);
    uint64_t e2 = modularExponent(g, d, p);

    printf("p = %lu\n", p);
    printf("g = %lu\n", g);
    printf("e2 = %lu\n", e2);
    printf("d = %lu\n", d);

    FILE *publickeyfile;
    FILE *privatekeyfile;

    publickeyfile = fopen(argv[1], "w");
    privatekeyfile = fopen(argv[2], "w");

    if(publickeyfile == NULL){
        perror("");
        exit(EXIT_FAILURE);
    }
    if(privatekeyfile == NULL){
        perror("");
        exit(EXIT_FAILURE);
    }

    fprintf(publickeyfile, "%lu %lu %lu", p, g, e2);    //output keys
    fprintf(privatekeyfile, "%lu %lu %lu", p, g, d);

    fclose(publickeyfile);
    fclose(privatekeyfile);
}


void encrypt(char *argv[]){
    //./crypto publickey.txt plaintext.txt -e
    
    srand(time(NULL));
    FILE *publickeyfile;
    int plaintextfile;
    int cyphertextfile;

    char line[300];

    publickeyfile = fopen(argv[1], "r"); 
    plaintextfile = open(argv[2], O_RDONLY);
    cyphertextfile = open(CYPHERTEXT, O_WRONLY|O_CREAT|O_TRUNC, 0644);

    if(publickeyfile == NULL){
        perror("");
        exit(EXIT_FAILURE);
    }
    if(plaintextfile == -1){
        perror("");
        exit(EXIT_FAILURE);
    }
    if(cyphertextfile == -1){
        perror("");
        exit(EXIT_FAILURE);
    }

    uint64_t g, e2, p;

    fgets(line, sizeof(line), publickeyfile);
    sscanf(line, "%lu %lu %lu", &p, &g, &e2);   //get keys

    uint64_t mblock = 0;
    while(read(plaintextfile, &mblock, 8) > 0){
        printf("m = %016" PRIx64 "\n",mblock);
        uint64_t k = randBetween(0, p -1);
        uint64_t c1 = modularExponent(g, k, p);
        printf("c1 = %" PRIu64 "\n",c1);
        uint64_t c2 = modMul(modularExponent(e2, k, p), mblock, p); //from sudo code
        printf("c2 = %" PRIu64 "\n",c2);
        if (dprintf(cyphertextfile,"%" PRIu64 " %" PRIu64 "\n", c1, c2) < 0) {
            perror("");
            exit(EXIT_FAILURE);
        }
    }

    fclose(publickeyfile);
    close(plaintextfile);
    close(cyphertextfile);

}

void decrypt(char* argv[]){
    //./crypto privatekey.txt cyphertext.txt -d
    
    FILE *privatekeyfile;
    int cyphertextfile;
    int outputfile;

    char line[300];

    privatekeyfile = fopen(argv[1], "r");
    cyphertextfile = open(argv[2], O_RDONLY);
    outputfile = open(OUTPUT, O_WRONLY|O_CREAT|O_TRUNC, 0644);

    if(privatekeyfile == NULL){
        perror("");
        exit(EXIT_FAILURE);
    }
    if(cyphertextfile == -1){
        perror("");
        exit(EXIT_FAILURE);
    }
    if(outputfile == -1){
        perror("");
        exit(EXIT_FAILURE);
    }

    uint64_t p, g, d;

    fgets(line, sizeof(line), privatekeyfile);
    sscanf(line, "%lu %lu %lu", &p, &g, &d);   //get keys

    while(true){
        char buffer[64] = {0}; //init
        for(int i = 0; i < 64; i++){
            ssize_t readBytes = read(cyphertextfile, &(buffer[i]), 1);
            if(readBytes == -1){
                perror("");
                exit(EXIT_FAILURE);
            }
            if(readBytes == 0){
                close(cyphertextfile);
                fclose(privatekeyfile);
                close(outputfile);
                exit(EXIT_SUCCESS);
            }
            if(buffer[i] == '\n'){
                break;
            }
        }

        char* ss = NULL;
        uint64_t c1 = (uint64_t) strtoull(buffer, &ss, 10);
        uint64_t c2 = (uint64_t) strtoull(ss, NULL, 10);
        uint64_t mblock = modMul(modularExponent(c1, p - 1 - d, p), c2, p); //from sudo code
        if (write(outputfile, &mblock, 8) == -1) {  //output to file
            perror("");
            exit(EXIT_FAILURE);
        }
    }

}

int main(int argc, char *argv[]){

   if(argc == 5){
       if(strcmp(argv[argc - 1], "-k") == 0){
            uint64_t seed;
            sscanf(argv[argc - 2], "%lu", &seed);
            keygen(seed, argv);
       }
   } 
   else if(argc == 4){
       if(strcmp(argv[argc - 1], "-e") == 0){
           encrypt(argv);
       }
       else if(strcmp(argv[argc - 1], "-d") == 0){
           decrypt(argv);
       }
   }
   else{
       printf("incorrect arguments\n");
   }
}
