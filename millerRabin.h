#include <stdbool.h>
#include <stdint.h>

uint64_t randBetween(uint64_t, uint64_t);

uint64_t modMul(__uint128_t, __uint128_t, __uint128_t);

uint64_t modularExponent(__uint128_t, __uint128_t, __uint128_t);

bool witness(uint64_t, uint64_t);

bool millerRabin(uint64_t, uint64_t);
