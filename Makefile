cryptosystem: cryptosystem.o millerRabin.o
	gcc -o cryptosystem cryptosystem.o millerRabin.o

cryptosystem.o: cryptosystem.c
	gcc -c cryptosystem.c

millerRabin.o: millerRabin.c
	gcc -c millerRabin.c

clean:
	rm cryptosystem cryptosystem.o millerRabin.o
