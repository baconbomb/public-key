#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>

uint64_t randBetween(uint64_t min, uint64_t max) {  //rand number between to values
    uint64_t r = (((uint64_t) rand()) << 32) | (((uint64_t) rand()) & UINT32_MAX);
    uint64_t result = min + (uint64_t) ((double)(max-min+1)*r/(UINT64_MAX+1.0));
    return result;
}

uint64_t modMul(__uint128_t a, __uint128_t b, uint64_t mod) {
    if ((a == 0) || (b < (mod / a))) return (a*b) % mod;
    uint64_t result = 0;
    a = a % mod;
    while (b > 0) {
        if ((b & 1) == 1) {
            result = (result + a) % mod;
        }
        a = (a<<1) % mod;
        b >>= 1;
    }
    return result;
}

uint64_t modularExponent(__uint128_t a, __uint128_t b, __uint128_t n){
    
    uint64_t result = 1;

    while (b > 0) {
        if ((b & 1) == 1) {
            result = (result * a) % n;
        }
        a = (a * a) % n;
        b >>= 1;
    }
    return result;    

}

bool millerRabin(uint64_t n, uint64_t s){

    if (n <=1 || n == 4){
        return false;
    }
    if(n <= 3){
        return true;
    }

    if(n % 2 == 0){
        return false;
    }

    int r = 0;
    uint64_t d = n - 1;

    while(d % 2 == 0){
        d = d / 2;
        r++;
    }

    for(int i = 0; i < s; i++){
        uint64_t a = randBetween(2, n - 2);
        uint64_t x = modularExponent(a, d, n);
        if((x == 1) || (x == n - 1)){
            continue;
        }
        for(int j = 1; j <= (r - 1); j++){
            x = modularExponent(x, 2, n);
            if(x == 1){
                return false;
            }
            if(x == n - 1){
                goto LOOP;
            }
        }
        return false;
LOOP:
        continue;
    }
    return true;
}
